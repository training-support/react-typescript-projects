import { useState } from "react";
import "./RollDice.css";
import Die from "./Die";

type RollDiceProps = {
  sides?: string[];
}

const RollDice = ({sides = ["one", "two", "three", "four", "five", "six"]} : RollDiceProps) => {
    // Set some initial state that will hold our dice values
    const [dice, setDice] = useState({ die1: "one", die2: "one" });
    const [isRolling, setIsRolling] = useState(false);

    // Define a function that'll handle the rolling and updating of the state
    const roll = () => {
        // Pick two new rolls
        const newDie1 = sides[Math.floor(Math.random() * sides.length)];
        const newDie2 = sides[Math.floor(Math.random() * sides.length)];

        // Set the state to be equal to the new rolls
        setDice({die1: newDie1, die2: newDie2});
        setIsRolling(true);

        // Wait one second and set isRolling to false again
        setTimeout(() => {
            setIsRolling(false);
        }, 1000);
    };

    return (
        <div className="RollDice">
            <div className="RollDice-Container">
                <Die face={dice.die1} rolling={isRolling}/>
                <Die face={dice.die2} rolling={isRolling}/>
            </div>
            <button onClick={roll}>Roll Dice!</button>
        </div>
    );
};

export default RollDice;
