import "./Die.css";

type DieProps = {
  face: string;
  rolling: boolean;
}

const Die = ({face, rolling}: DieProps) => {
    return (
        <i className={`Die fas fa-dice-${face} ${rolling ? "rolling" : ""}`}></i>
    );
};

export default Die;
