import { useEffect, useState } from "react";
import axios from "axios";
import { Card } from "../Card";
import "./Deck.css";

const API_BASE_URL = "https://deckofcardsapi.com/api/deck";

type Deck = {
    success: boolean;
    deck_id: string;
    remaining: number;
    shuffled: boolean;
}

type CardDraw = {
    success: boolean;
    deck_id: string;
    cards: Card[];
    remaining: number;
}

type Card = {
    code: string;
    image: string;
    images: {
        svg: string,
	png: string
    },
    value: string;
    suit: string;
}

type DrawnCard = {
    id: string;
    image: string;
    name: string;
}

export function Deck() {
    const [deck, setDeck] = useState({} as Deck);
    const [drawn, setDrawn] = useState([] as DrawnCard[]);

    async function getDeck() {
        const deck = await axios.get(`${API_BASE_URL}/new/shuffle`);
        setDeck(deck.data);
    }

    useEffect(() => {
        getDeck();
    }, []);

    async function getCard() {
        const deck_id = deck.deck_id;
        try {
            const cardUrl = `${API_BASE_URL}/${deck_id}/draw`;
            const cardRes = await axios.get<CardDraw>(cardUrl);
            if(!cardRes.data.success) {
                throw new Error("No cards remaining!");
            }
            const card = cardRes.data.cards[0];
            setDrawn(st => [...st, {id: card.code, image: card.image, name: `${card.value} of ${card.suit}`}]);
        } catch (err) {
            alert(err);
        }
    }

    const cards = drawn.map(card => (
        <Card key={card.id} name={card.name} image={card.image} />
    ));

    return (
        <div>
            <h1 className="Deck-title">Card Dealer</h1>
            <button className="Deck-btn" onClick={getCard}>Get Card!</button>
            <div className="Deck-cardarea">{cards}</div>
        </div>
    );
}
