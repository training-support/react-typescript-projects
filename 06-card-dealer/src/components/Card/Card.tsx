import { useState } from "react";
import "./Card.css";

type CardProps = {
    image: string;
    name: string;
}

export function Card({image, name}: CardProps) {
    const angle = useState(Math.random() * 90 - 45)[0];
    const xPos = useState(Math.random() * 40 - 20)[0];
    const yPos = useState(Math.random() * 40 - 20)[0];
    const _transform = `translate(${xPos}px, ${yPos}px) rotate(${angle}deg)`;
    
    return (
        <img 
            className="Card" 
            src={image} 
            alt={name}
            style={{ transform: _transform }}
        />
    );
}
