import Pokecard from "./Pokecard";
import "./Pokedex.css";

type Props = {
  pokemon: PokemonDetails[],
  exp: number,
  isWinner: boolean,
}

type PokemonDetails = {
  id: number,
  name: string,
  type: string,
  base_experience: number,
}

const Pokedex = ({ pokemon, exp, isWinner}: Props) => {
    let title;
    if(isWinner) {
        title = <h1 className="Pokedex-winner">Winner!</h1>;
    } else {
        title = <h1 className="Pokedex-loser">Loser!</h1>;
    }
    return (
        <div className="Pokedex">
            {title}
            <h4>Total Experience: {exp}</h4>
            <div className="Pokedex-cards">
                {pokemon.map((p) => (
                    <Pokecard
                        key={p.id}
                        id={p.id}
                        name={p.name}
                        type={p.type}
                        exp={p.base_experience}
                    />
                ))}
            </div>
        </div>
    );
};

export default Pokedex;
