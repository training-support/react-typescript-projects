import { useState, useEffect } from "react";
import axios from "axios";
import { v4 as uuid } from "uuid";
import { Joke } from "../Joke";
import "./JokesList.css";

type Joke = {
    id: string;
    text: string;
    votes: number;
}

type JokesListProps = {
    numJokesToGet?: number;
}

export function JokesList({numJokesToGet = 10}: JokesListProps) {
    const [jokes, setJokes] = useState(JSON.parse(window.localStorage.getItem("jokes") || "[]") as Joke[]);
    const [isLoading, setIsLoading] = useState(false);
    const seenJokes = new Set(jokes.map((joke) => joke.text));

    useEffect(() => {
        if(jokes.length === 0) { getJokes(); }
    }, []);

    async function getJokes() {
        try {
            const newJokes: Joke[] = [];
	
            while(newJokes.length < numJokesToGet) {
                const res = await axios.get("https://icanhazdadjoke.com/", {
                    headers: { Accept: "application/json" },
                });
                const newJoke = res.data.joke;
                if(!seenJokes.has(newJoke)) {
                    newJokes.push({id: uuid(), text: res.data.joke, votes: 0});
                } else {
                    console.log("FOUND A DUPLICATE!");
                    console.log(newJoke);
                }
            }
            setIsLoading(false);
            setJokes(st => [...st, ...newJokes]);
            window.localStorage.setItem("jokes", JSON.stringify(jokes));
        } catch (err) {
            alert(err);
            setIsLoading(false);
        }
    }

    function handleVote(id: string, delta: number) {
        setJokes(st => (
            st.map((joke) => joke.id === id ? { ...joke, votes: joke.votes + delta } : joke)
        ));
        window.localStorage.setItem("jokes", JSON.stringify(jokes));
    }

    function handleClick() {
        setIsLoading(true);
        getJokes();
    }

    const sortedJokes = jokes.sort((a, b) => b.votes - a.votes);

    if(isLoading) {
        return (
            <div className="JokeList-spinner">
                <i className="far fa-8x fa-laugh fa-spin" />
                <h1 className="JokeList-title">Loading...</h1>
            </div>
        );
    }

    return (
        <div className="JokeList">
            <div className="JokeList-sidebar">
                <h1 className="JokeList-title">
                    <span>Joke</span> Generator
                </h1>
                <img
                    src="https://assets.dryicons.com/uploads/icon/svg/8927/0eb14c71-38f2-433a-bfc8-23d9c99b3647.svg"
                    alt="Laughing Emoji"
                />
                <button className="JokeList-getmore" onClick={handleClick}>New Jokes!</button>
            </div>
            <div className="JokeList-jokes">
                {sortedJokes.map((joke) => (
                    <Joke
                        key={joke.id}
                        votes={joke.votes}
                        text={joke.text}
                        upvote={() => handleVote(joke.id, 1)}
                        downvote={() => handleVote(joke.id, -1)}
                    />
                ))}
            </div>
        </div>
    );

}
