import { useState } from "react";
import "./Hangman.css";
import { randomWord } from "./data/words";
import img0 from "./assets/0.jpg";
import img1 from "./assets/1.jpg";
import img2 from "./assets/2.jpg";
import img3 from "./assets/3.jpg";
import img4 from "./assets/4.jpg";
import img5 from "./assets/5.jpg";
import img6 from "./assets/6.jpg";

type HangmanProps = {
    maxWrong?: number;
    images?: string[];
};

function Hangman({
    maxWrong = 6,
    images = [img0, img1, img2, img3, img4, img5, img6],
}: HangmanProps) {

    /** Initialize State*/
    const [gameState, setGameState] = useState({ nWrong: 0, guessed: new Set(), answer: randomWord() });

    /** guessedWord: show current-state of the word:
    	if guessed letters are {a, p, e}, show "app_e" for "apple" 
    */
    function guessedWord() {
        return gameState.answer.split("").map(letter => gameState.guessed.has(letter) ? letter : "_");
    }

    /** handleGuess: handle a guessed letter:
        - add to guessed letters
	- if not in answer, increase number of wrong guesses
    */
    function handleGuess(evt: React.MouseEvent<HTMLButtonElement>) {
        evt.preventDefault();
        const element = evt.target as HTMLButtonElement;
        const letter: string = element.value;
        setGameState(st => ({
            ...st,
            guessed: st.guessed.add(letter),
            nWrong: st.nWrong + (st.answer.includes(letter) ? 0 : 1)
        }));
    }

    /** generateButtons: return an array of letter buttons to render */
    function generateButtons() {
        return "abcdefghijklmnopqrstuvwxyz".split("").map(letter => (
            <button
                key={letter}
                value={letter}
                onClick={handleGuess}
                disabled={gameState.guessed.has(letter)}
            >{letter}</button>
        ));
    }

    /** Reset the game */
    function reset() {
        setGameState({
            nWrong: 0,
            guessed: new Set(),
            answer: randomWord()
        });
    }

    /** Check if the game has ended */
    const gameOver = gameState.nWrong >= maxWrong;

    /** Construct the alt text for the images */
    const altText = `${gameState.nWrong}/${maxWrong} guesses done!`;

    /** Check if the player won */
    const isWinner = guessedWord().join("") === gameState.answer;

    /** Render the game area */
    const renderGameArea = () => {
        if (isWinner) return <p>You Win!</p>;
        if (gameOver) return <p>You Lose!</p>;
        return generateButtons();
    };

    /** render the game */
    return (
        <div className="Hangman">
            <h1>Hangman!</h1>
            <img src={images[gameState.nWrong]} alt={altText} />
            <p>Guessed Wrong: {gameState.nWrong}</p>
            <p className="Hangman-word">
                {!gameOver ? guessedWord() : gameState.answer}
            </p>
            <p className="Hangman-btns">
                {renderGameArea()}
            </p>
            <button id="reset" onClick={reset}>Restart?</button>
        </div>
    );
}

export default Hangman;
