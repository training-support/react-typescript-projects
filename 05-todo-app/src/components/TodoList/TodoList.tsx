import { useState } from "react";
import { Todo } from "../Todo";
import { NewTodoForm } from "../NewTodoForm";
import "./TodoList.css";

type Todo = {
    task: string;
    id: string;
    completed: boolean;
}

export function TodoList() {
    const [todoList, setTodoList] = useState([] as Todo[]);

    function create(newTodo: Todo) {
        setTodoList([...todoList, newTodo]);
    }

    function update(id: string, updatedTask: string) {
        const updatedTodos = todoList.map(todo => {
            if(todo.id === id) {
                return {...todo, task: updatedTask};
            }
            return todo;
        });
        setTodoList(updatedTodos);
    }

    function toggleCompletion(id: string) {
        const updatedTodos = todoList.map(todo => {
            if(todo.id === id) {
                return {...todo, completed: !todo.completed};
            }
            return todo;
        });
        setTodoList(updatedTodos);
    }

    function remove(id: string) {
        setTodoList(todoList.filter(todo => todo.id !== id));
    }

    /** Generate the Todos */
    const todos = todoList.map((todo) => (
        <Todo 
            key={todo.id} 
            id={todo.id} 
            completed={todo.completed}
            task={todo.task} 
            removeTodo={remove} 
            updateTodo={update}
            toggleTodo={toggleCompletion}
        />)
    );

    return (
        <div className="TodoList">
            <h1>Todo List!</h1>
            <ul>{todos}</ul>
            <NewTodoForm createTodo={create} />
        </div>
    );
}
