import { ChangeEvent, FormEvent, useState } from "react";
import { v4 as uuid } from "uuid";
import "./NewTodoForm.css";

type Task = {
    task: string;
    id: string;
    completed: boolean;
}

type NewTodoFormProps = {
    createTodo: (newTodo: Task) => void;
}

export function NewTodoForm({createTodo}: NewTodoFormProps) {
    const [task, setTask] = useState({} as Task);

    function handleChange(evt: ChangeEvent<HTMLInputElement>) {
        const newTask = {
            ...task,
            [evt.target.name]: evt.target.value,
        };
        setTask(newTask);
    }

    function handleSubmit(evt: FormEvent) {
        evt.preventDefault();
        createTodo({...task, id: uuid(), completed: false});
        setTask({ task: "", id: "", completed: false });
    }

    return (
        <form className="NewTodoForm" onSubmit={handleSubmit}>
            <label htmlFor="task">New Todo: </label>
            <input
                type="text"
                placeholder="New Todo"
                id="task"
                name="task"
                value={task.task}
                onChange={handleChange}
            />
            <button>Add Todo</button>
        </form>
    );
}
