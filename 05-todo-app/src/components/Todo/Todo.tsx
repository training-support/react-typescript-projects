import { ChangeEvent, FormEvent, useState } from "react";
import "./Todo.css";

type TodoProps = {
    task: string;
    id: string;
    completed: boolean;
    removeTodo: (id: string) => void;
    updateTodo: (id: string, task: string) => void;
    toggleTodo: (id: string) => void;
}

export function Todo({task, id, completed, removeTodo, updateTodo, toggleTodo}: TodoProps) {
    const [isEditing, setIsEditing] = useState(false);
    const [currentTask, setCurrentTask] = useState(task);

    function handleRemove() {
        removeTodo(id);
    }

    function toggleForm() {
        setIsEditing(!isEditing);
    }

    function handleUpdate(evt: FormEvent) {
        evt.preventDefault();
        updateTodo(id, currentTask);
        setIsEditing(false);
    }

    function handleChange(evt: ChangeEvent<HTMLInputElement>) {
        setCurrentTask(evt.target.value);
    }

    function handleToggle() {
        toggleTodo(id);
    }

    let result;
    if(isEditing) {
        result = (
            <div className="Todo">
                <form className="Todo-edit-form" onSubmit={handleUpdate}>
                    <input type="text" value={currentTask} name="task" onChange={handleChange}/>
                    <button><i className="fas fa-save"/></button>
                </form>
            </div>
        );
    } else {
        result = (
            <div className="Todo">
                <li className={completed ? "Todo-task completed" : "Todo-task"} onClick={handleToggle}>{task}</li>
                <div className="Todo-buttons">
                    <button onClick={toggleForm}><i className="fas fa-pen" /></button>
                    <button onClick={handleRemove}><i className="fas fa-trash"/></button>
                </div>
            </div>
        );
    }

    return result;
}
