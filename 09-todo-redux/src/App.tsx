import Todos from "./components/Todos";
import TodoForm from "./components/TodoForm";
import { useAppSelector } from "./app/hooks";
import "./App.css";

function App() {
    const pendingTasks = useAppSelector((state) => state.todos.data.length);

    return (
        <div className="container">
            <h1>Todo App</h1>
            <TodoForm />
            <Todos />
            <span className="pending">
                {pendingTasks} tasks remain.
            </span>
        </div>
    );
}

export default App;
