import { useAppDispatch } from "../app/hooks";
import { removeTodo } from "../features/todos/todosSlice";
import "./Todo.css";

interface TodoProps {
    text: string;
    id: number;
}

function Todo({text, id}: TodoProps) {
    const dispatch = useAppDispatch();

    function removeHandler() {
        dispatch(removeTodo(id));
    }

    return (
        <div className="Todo">
            <span>{text}</span>
            <i className="fa-solid fa-xmark" onClick={removeHandler} />
        </div>
    );
}

export default Todo;
