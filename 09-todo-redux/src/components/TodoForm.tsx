import { FormEvent, useState } from "react";
import { useAppDispatch } from "../app/hooks";
import { addTodo } from "../features/todos/todosSlice";
import "./TodoForm.css";

function TodoForm() {
    const dispatch = useAppDispatch();
    const [text, setText] = useState("");

    function changeHandler(e: FormEvent<HTMLInputElement>) {
        setText(e.currentTarget.value);
    }

    function submitHandler(e: FormEvent) {
        e.preventDefault();

        dispatch(addTodo(text));
        setText("");
    }

    return (
        <form className="TodoForm" onSubmit={submitHandler}>
            <div className="TodoForm-add">
                <input
                    onChange={changeHandler}
                    value={text}
                    placeholder="Add new todo here"
                />
                <button>
                    <i className="fa-solid fa-plus fa-2xl" />
                </button>
            </div>
        </form>
    );
}

export default TodoForm;
