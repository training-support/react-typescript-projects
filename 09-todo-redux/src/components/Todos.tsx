import { useAppSelector } from "../app/hooks";
import Todo from "./Todo";
import "./Todos.css";

function Todos() {
    const todos = useAppSelector((state) => state.todos.data);

    const todoList = todos.map((todo) => (
        <Todo key={todo.id} text={todo.text} id={todo.id} />
    ));

    return <div className="Todos">{todoList}</div>;
}

export default Todos;
