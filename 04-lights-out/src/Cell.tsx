import "./Cell.css";

/** A single cell on the board.
 *
 * This has no state --- just two props:
 *
 * - flipCellsAroundMe: a function rec'd from the board which flips this
 *      cell and the cells around of it
 *
 * - isLit: boolean, is this cell lit?
 *
 * This handles clicks --- by calling flipCellsAroundMe
 *
 **/

interface CellProps {
    flipCellsAroundMe: (coords?: string) => void;
    isLit: boolean;
}

function Cell({flipCellsAroundMe, isLit}: CellProps) {
    const handleClick = () => {
        // Call up to the board to flip this cell and ones around it
        flipCellsAroundMe();
    };

    const classes = "Cell" + (isLit ? " Cell-lit" : "");
    return (
        <td className={classes} onClick={handleClick} />
    );
}

export default Cell;
