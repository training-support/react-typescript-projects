import { useState } from "react";
import Cell from "./Cell";
import "./Board.css";

/** Game board of Lights out.
 *
 * Properties:
 *
 * - nrows: number of rows of board
 * - ncols: number of cols of board
 * - chanceLightStartsOn: float, chance any cell is lit at start of game
 *
 * State:
 *
 * - hasWon: boolean, true when board is all off
 * - board: array-of-arrays of true/false
 *
 *    For this board:
 *       .  .  .
 *       O  O  .     (where . is off, and O is on)
 *       .  .  .
 *
 *    This would be: [[f, f, f], [t, t, f], [f, f, f]]
 *
 *  This should render an HTML table of individual <Cell /> components.
 *
 *  This doesn't handle any clicks --- clicks are on individual cells
 *
 **/

interface BoardProps {
    nrows?: number;
    ncols?: number;
    chanceLightStartsOn?: number;
}

function Board({nrows = 5, ncols = 5, chanceLightStartsOn = 0.25}: BoardProps) {

    const [hasWon, setHasWon] = useState(false);
    const [boardState, setBoardState] = useState(createBoard());


    /** create a board nrows high/ncols wide, each cell randomly lit or unlit */
    function createBoard() {
        const board = [];

        for(let y = 0; y < nrows; y++) {
            const row = [];
            for(let x = 0; x < ncols; x++) {
                row.push(Math.random() < chanceLightStartsOn);
            }
            board.push(row);
        }

        return board;

    }

    /** handle changing a cell: update board & determine if winner */
    function flipCellsAround(coord: string) {
        console.log("FLIPPING", coord);
        const board = boardState; // `boardState` is coming from the state you set
        const [y, x] = coord.split("-").map(Number);

        function flipCell(y: number, x: number) {
            
            // if this coord is actually on board, flip it
            if (x >= 0 && x < ncols && y >= 0 && y < nrows) {
                board[y][x] = !board[y][x];
            }
        }

        flipCell(y, x);
        flipCell(y, x - 1);
        flipCell(y, x + 1);
        flipCell(y - 1, x);
        flipCell(y + 1, x);

        // win when every cell is turned off
        setBoardState([...board]);
        setHasWon(board.every(row => row.every(cell => !cell)));
    }

    /** Render game board or winning message. */

    // if the game is won, just show a winning msg & render nothing else
    if(hasWon) {
        return (
            <div className='Board-title'>
                <div className='winner'>
                    <span className='neon-orange'>YOU</span>
                    <span className='neon-blue'>WIN</span>
                </div>
            </div>
        );
    }

    const tableBoard = [];
    for(let y = 0; y < nrows; y++) {
        const row = [];
        for(let x = 0; x < ncols; x++) {
            const coord = `${y}-${x}`;
            row.push(<Cell key={coord} isLit={boardState[y][x]} flipCellsAroundMe={() => flipCellsAround(coord)} />);
        }
        tableBoard.push(<tr key={y}>{row}</tr>);
    }
    return (
        <div>
            <div className='Board-title'>
                <div className='neon-orange'>Lights</div>
                <div className='neon-blue'>Out</div>
            </div>
            <table className="Board">
                <tbody>{tableBoard}</tbody>
            </table>
        </div>
    );
}

export default Board;
