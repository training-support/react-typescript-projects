import { Link } from "react-router-dom";
import { Place } from "./App";
import "./PlaceList.css";

type PlaceListProps = {
    places: Place[];
}

export default function PlaceList({places}: PlaceListProps) {
    return (
        <div className="PlaceList">
            <h1 className="display-1 text-center my-5">Places!</h1>
            <div className="row">
                {places.map((place) => (
                    <div className="PlaceList-place col-lg-4 text-center" key={place.name}>
                        <img src={place.src} alt={place.name} />
                        <h3>
                            <Link className="underline" to={`/places/${place.name}`}>{place.name}</Link>
                        </h3>
                    </div>
                ))}
            </div>
        </div>
    );
}
