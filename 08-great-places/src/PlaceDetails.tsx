import { Place } from "./App";
import { Link, useParams } from "react-router-dom";
import "./PlaceDetails.css";

type PlaceDetailsProps = {
    places: Place[];
}

export default function PlaceDetails({places}: PlaceDetailsProps) {
    const urlParams = useParams();
    const name = urlParams.name;
    const currentPlace = places.find(
        (place) => place.name.toLowerCase() === name?.toLowerCase()
    );

    if(currentPlace !== undefined) {
        return (
            <div className="PlaceDetails row justify-content-center mt-5">
                <div className="col-11 col-lg-5">
                    <div className="PlaceDetails-card card">
                        <img className="card-img-top" src={currentPlace.src} alt={currentPlace.name} />
                        <div className="card-body">
                            <h2 className="card-title">{currentPlace.name}</h2>
                            <h4 className="card-subtitle">{currentPlace.location}</h4>
                        </div>
                        <ul className="list-group list-group-flush">
                            {currentPlace.facts.map((fact, idx) => (
                                <li className="list-group-item"key={idx}>
                                    {fact}
                                </li>
                            ))}
                        </ul>
                        <div className="card-body">
                            <Link to="/places" className="btn btn-info">Go Back</Link>
                        </div>
                    </div>
                </div>
            </div>
        );
    } else {
        return <h1>{"You're trying to see an invalid place."}</h1>;
    }
}
