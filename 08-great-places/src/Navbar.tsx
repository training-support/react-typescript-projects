import { NavLink, Link } from "react-router-dom";
import { Place } from "./App";

type NavProps = {
    places: Place[];
}

export default function Navbar({places}: NavProps) {
    const placeLinks = places.map((place) => (
        <li className="nav-item" key={place.name}>
            <NavLink
                to={`/places/${place.name}`}
                className="nav-link"
            >
                {place.name}
            </NavLink>
        </li>
    ));
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <Link className="navbar-brand" to="/places">
                Great Places App
            </Link>
            <button
                className="navbar-toggler"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#navbarNav"
                aria-controls="navbarNav"
                aria-expanded="false"
                aria-label="Toggle navigation"
            >
                <span className="navbar-toggler-icon" />
            </button>
            <div id="navbarNav" className="collapse navbar-collapse">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <NavLink to="/places" className="nav-link">
                            Home
                        </NavLink>
                    </li>
                    {placeLinks}
                </ul>
            </div>
        </nav>
    );
}
