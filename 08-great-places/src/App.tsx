import { Route, Routes } from "react-router-dom";
import "./App.css";
import PlaceList from "./PlaceList";
import PlaceDetails from "./PlaceDetails";
import tajMahal from "./images/TajMahal.jpg";
import colosseum from "./images/Colosseum.jpg";
import machuPicchu from "./images/MachuPicchu.jpg";
import Navbar from "./Navbar";

type AppProps = {
    places?: Place[];
}

export type Place = {
    name: string;
    location: string;
    src: string;
    facts: string[];
}

function App({
    places = [
        {
            name: "Taj Mahal",
            location: "India",
            src: tajMahal,
            facts: [
                "The Taj Mahal is well known across the world for its historical value, its tale of love, and its stunning beauty.",
                "The Taj Mahal is located in the historic Indian city of Agra.",
                "It houses the tomb of Mumtaz Mahal, the wife of the Mughal Emperor Shah Jahan.",
                "It is said that the Emperor loved his wife dearly and was prompted to build the Taj Mahal after her death as a testament to his love.",
            ]
        },
        {
            name: "The Colosseum",
            location: "Italy",
            src: colosseum,
            facts: [
                "Sometimes called the Flavian Amphitheatre, the Colosseum is an oval-shaped amphitheater in the center of Rome, Italy.",
                "Constructed from concrete and sand, it is the largest amphitheater in the world.",
                "The Colosseum's construction was initiated in AD 72 by Emperor Vespasian and was finished by AD 80 by his successor, Titus.",
                "The Colosseum had the capacity to host about 80,000 spectators and 80 entrances at the time of its construction.",
            ]
        },
        {
            name: "Machu Picchu",
            location: "Peru",
            src: machuPicchu,
            facts: [
                "The dream destination of millions of people across the world, Machu Picchu is one of the seven wonders of the world.",
                "It is located in the Cusco Region of Peru’s Machupicchu District. According to the majority of archaeologists, the Inca emperor Pachacuti built Machu Picchu as an estate around the year 1450.",
                "The site developed as a city but was abandoned a century later during the Spanish Conquest.",
                "The site remained largely unknown to the rest of the world until its discovery by the American explorer Hiram Bingham.",
            ]
        },
    ]
}: AppProps) {
    
    return (
        <div>
            <Navbar places={places}/>
            <div className="container">
                <Routes>
                    <Route path="/places" element={<PlaceList places={places} />} />
                    <Route path="/places/:name" element={<PlaceDetails places={places}/>} />
                </Routes>
            </div>
        </div>
    );
}

export default App;
